#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

This file is part of **python-openzwave** project https://github.com/OpenZWave/python-openzwave.
    :platform: Unix, Windows, MacOS X
    :sinopsis: openzwave wrapper

    .. moduleauthor:: bibi21000 aka Sébastien GALLET <bibi21000@gmail.com>

License : GPL(v3)

**python-openzwave** is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

**python-openzwave** is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with python-openzwave. If not, see http://www.gnu.org/licenses.

"""

import logging
import sys
import datetime
import time

logging.basicConfig(level=logging.INFO)

logger = logging.getLogger('openzwave')

from openzwave.controller import ZWaveController
from openzwave.network import ZWaveNetwork
from openzwave.option import ZWaveOption
import time
from louie import dispatcher
import sqlite3

device = "/dev/ttyACM0"
log = "warning"

volt = None
watt = None
kwh = None
node_var = None
amp = None

for arg in sys.argv:
    if arg.startswith("--device"):
        temp, device = arg.split("=")
    elif arg.startswith("--log"):
        temp, log = arg.split("=")
    elif arg.startswith("--help"):
        print("help : ")
        print("  --device=/dev/yourdevice ")
        print("  --log=Info|Debug")

# Define some manager options
options = ZWaveOption(device, config_path="../openzwave/config", user_path=".", cmd_line="")
options.set_log_file("OZW_Log.log")
options.set_append_log_file(False)
options.set_console_output(False)
options.set_save_log_level('Info')
options.set_logging(False)
options.lock()

def louie_network_started(network):
    print('//////////// ZWave network is started ////////////')
    print('Louie signal : OpenZWave network is started : homeid {:08x} - {} nodes were found.'.format(network.home_id, network.nodes_count))



def louie_network_resetted(network):
    pass

def set_poll_interval():
    pass


def louie_network_ready(network):
    dispatcher.connect(louie_node_update, ZWaveNetwork.SIGNAL_NODE)
    dispatcher.connect(louie_value_update, ZWaveNetwork.SIGNAL_VALUE)
    dispatcher.connect(louie_ctrl_message, ZWaveController.SIGNAL_CONTROLLER)

def louie_node_update(network, node):
    print('Louie signal : Node update : {}.'.format(node))

def louie_value_update(network, node, value):
    #print('Value update : {}.'.format(value))
    splitvalue = format(value).split()


    if splitvalue[7] == "[Energy]":
        global kwh
        kwh = str(splitvalue[9])
        kwh = round(float(kwh[1:-1]), 2)

    if splitvalue[7] == "[Voltage]":
        global volt
        volt = str(splitvalue[9])
        volt = round(float(volt[1:-1]), 2)

    if splitvalue[7] == "[Power]":
        global watt
        watt = str(splitvalue[9])
        watt = round(float(watt[1:-1]), 2)

    if splitvalue[7] == "[Current]":
        global amp
        amp = str(splitvalue[9])
        amp = round(float(amp[1:-1]), 2)

    global node_var
    node_var = str(splitvalue[5])
    node_var = node_var[1:-1]

    ts = str(time.strftime('%a %H:%M:%S'))

    if node_var != None and kwh != None and volt != None and watt != None and amp != None:
        print("---------------------------------------------")
        print("Node %s = kWh %s - V %s - A %s - Watt %s (%s)" %(node_var,kwh,volt,amp,watt,ts))
        print("---------------------------------------------")

        #send data to server
        # Create a database in RAM
        db = sqlite3.connect(':memory:')
        # Creates or opens a file called mydb with a SQLite3 DB
        db = sqlite3.connect('db.sqlite3')
        # Get a cursor object
        cursor = db.cursor()
        cursor.execute("SELECT * FROM devices_device WHERE id=?", (node_var,))

        rows = cursor.fetchall()

        if len(rows) == 0:
            print("New device found, added to DB : " + "Device: " + node_var)
            cursor.execute("INSERT INTO devices_device(id,name) VALUES (?, ?)", (node_var, "Device: " + node_var))
            db.commit()

        cursor.execute("INSERT INTO devices_measurement(id,voltage,ampere,kwh,watt,created_at,device_id) VALUES (?, ?, ?, ?, ?, ?, ?)", \
                       (None, volt, amp, kwh, watt, datetime.datetime.now(), node_var))
        db.commit()

        volt = None
        watt = None
        kwh = None
        node_var = None
        amp = None
        db.close()

def louie_ctrl_message(state, message, network, controller):
    print('Louie signal : Controller message : {}.'.format(message))


# Create a network object
network = ZWaveNetwork(options, log=None)

dispatcher.connect(louie_network_started, ZWaveNetwork.SIGNAL_NETWORK_STARTED)
dispatcher.connect(louie_network_resetted, ZWaveNetwork.SIGNAL_NETWORK_RESETTED)
dispatcher.connect(louie_network_ready, ZWaveNetwork.SIGNAL_NETWORK_READY)


for i in range(0, 300):
    if network.state >= network.STATE_STARTED:
        print(" Network is started")
        break
    else:
        sys.stdout.write(".")
        sys.stdout.flush()
        time.sleep(1.0)
if network.state < network.STATE_STARTED:
    print(".")
    print("Can't initialise driver! Look at the logs in OZW_Log.log")
    quit(1)
for i in range(0, 300):
    if network.state >= network.STATE_READY:
        print(" Network is ready")
        break
    else:
        sys.stdout.write(".")
        sys.stdout.write(network.state_str)
        sys.stdout.write("(")
        sys.stdout.write(str(network.nodes_count))
        sys.stdout.write(")")
        sys.stdout.write(".")
        sys.stdout.flush()
        time.sleep(1.0)
if not network.is_ready:
    print(".")
    print("Can't start network! Look at the logs in OZW_Log.log")
    quit(2)


while True:
    time.sleep(10)